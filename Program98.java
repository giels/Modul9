public class Program98 implements Runnable{
	Thread thread;
	
	public Program98(String name) {
		thread = new Thread(this, name);
		thread.start();
	}
	
	@Override
	public void run() {
		String name = thread.getName();
		for(int i=0; i<3; i++) {
			System.out.println(name);
		}
	}
	
	public static void main(String[] args) {
		Program98 t1 = new Program98("Thread 1");
		Program98 t2 = new Program98("Thread 2");
		Program98 t3 = new Program98("Thread 3");
		Program98 t4 = new Program98("Thread 4");
		System.out.println("Runnabel threads...");
		try {
			t1.thread.join();
			t2.thread.join();
			t3.thread.join();
			t4.thread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Threads killed");
		
	}
}
