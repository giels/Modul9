public class TwoStrings2 {

	synchronized static void print(String str1, String str2) {
		System.out.println(str1);
		try {
			Thread.sleep(1000);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(str2);
	}
	
	public static void main(String[] args) {
		new A("Hello ", "there.");
		new A("How are ", "you?");
		new A("Thank you ", "very much!");
	}

}

class A implements Runnable {

	Thread thread;
	String str1, str2;
	
	public A(String str1, String str2) {
		this.str1 = str1;
		this.str2 = str2;
		thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void run() {
		TwoStrings2.print(str1, str2);
	}
	
}
