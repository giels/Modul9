public class PrintNameThread extends Thread{
	
	public PrintNameThread(String name){
		super(name);
		start();
	}
	
	public void run() {
		String name = getName();
		for(int i=0; i<100; i++) {
			System.out.println(name);
		}
	}
}
class TestThread{
	public static void main(String[] args) {
		PrintNameThread pnt1 = new PrintNameThread("A");
		PrintNameThread pnt2 = new PrintNameThread("B");
		PrintNameThread pnt3 = new PrintNameThread("C");
		PrintNameThread pnt4 = new PrintNameThread("D");
	}
}
