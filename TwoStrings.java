public class TwoStrings {

	static void print(String str1, String str2) {
		System.out.println(str1);
		try {
			Thread.sleep(1000);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(str2);
	}
	
	public static void main(String[] args) {
		new PrintStringsThread("Hello ", "there.");
		new PrintStringsThread("How are ", "you?");
		new PrintStringsThread("Thank you ", "very much!");
	}

}

class PrintStringsThread implements Runnable {

	Thread thread;
	String str1, str2;
	
	public PrintStringsThread(String str1, String str2) {
		this.str1 = str1;
		this.str2 = str2;
		thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void run() {
		TwoStrings.print(str1, str2);
	}
	
}
