import java.awt.BorderLayout;
import java.awt.Panel;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class CountDownGUI extends JFrame{
	
	JLabel label;
	
	public CountDownGUI(String title) {
		super(title);
		label = new JLabel("Start count");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().add(new Panel(), BorderLayout.NORTH);
		getContentPane().add(label);
		setSize(300,300);
		setVisible(true);
	}
	
	public void startCount() {
		try {
			for(int i=10; i> 0; i--) {
				Thread.sleep(1000);
				label.setText(i+"");
			}
			Thread.sleep(1000);
			label.setText("Countdown Complete.");
			Thread.sleep(1000);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		label.setText(Thread.currentThread().toString());
	}
	
	public static void main(String[] args) {
		CountDownGUI cdg = new CountDownGUI("CountDown GUI");
		cdg.startCount();
	}

}
