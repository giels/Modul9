  public class SharedData {

	int data;
	
	synchronized void set(int value) {
		System.out.println("Generate "+value);
		data = value;
	}
	
	synchronized int get() {
		System.out.println("Get "+data);
		return data;
	}
	
	public static void main(String[] args) {
		SharedData sd = new SharedData();
		new Producer(sd);
		new Consumer(sd);
	}

}

class Producer implements Runnable{

	SharedData sd;
	
	public Producer(SharedData sd) {
		this.sd = sd;
		new Thread(this, "Producer").start();
	}
	
	@Override
	public void run() {
		for(int i=0; i<3; i++) {
			sd.set((int)(Math.random()*1000));
		}
	}
	
}

class Consumer implements Runnable{

	SharedData sd;
	public Consumer(SharedData sd) {
		this.sd = sd;
		new Thread(this, "Consumer").start();
	}
	
	@Override
	public void run() {
		for(int i=0; i<3; i++) {
			sd.get();
		}
	}
	
}
