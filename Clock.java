import java.applet.Applet;
import java.awt.Graphics;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class Clock extends Applet implements Runnable{

	private Thread clockThread = null;
	
	public void start() {
		if(clockThread == null) {
			clockThread = new Thread(this, "Clock");
			clockThread.start();
		}
	}
	
	@Override
	public void run() {
		Thread myThread = Thread.currentThread();
		while(clockThread == myThread) {
			repaint();
			try {
				Thread.sleep(1000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void paint(Graphics g) {
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();
		DateFormat dateFormat = DateFormat.getTimeInstance();
		g.drawString(dateFormat.format(date), 5, 10);
	}
	
	public void stop() {
		clockThread = null;
	}}
